import 'dotenv/config';
console.log(process.env);

import express from 'express';
const app = express();
//
import http from 'http';
const server = http.createServer(app);
//
import {fileURLToPath} from 'url';
import path from 'path';
//
import {Server} from 'socket.io';
const io = new Server(server, {
	cors: {
		origin:
			process.env.NODE_ENV === 'prod'
				? false
				: ['http://localhost:5500', 'https://eliazoura.fr'],
	},
});

//
const __filename = fileURLToPath(import.meta.url);
console.log('🚀 ~ __filename:', __filename);
const __dirname = path.dirname(__filename);

console.log('DirName', __dirname); // Affiche le répertoire parent du module actuel

//route static
const publicDirectoryPath = path.join(__dirname, '..', 'front', 'public');
console.log('🚀 ~ publicDirectoryPath:', publicDirectoryPath);

app.use(express.static(publicDirectoryPath));
//
app.get('/', (req, res) => {
	const filePath = path.join(__dirname, '..', 'front', 'index.html');
	res.sendFile(filePath);
});

app.get('/chat', (req, res) => {
	const filePath = path.join(__dirname, '..', 'front', 'chat.html');
	res.sendFile(filePath);
});

io.on('connection', (socket) => {
	// console.log('🚀 ~ io.on ~ socket:', socket);
	const time = new Date().toLocaleTimeString();
	//
	const someUserid = socket.id.substring(0, 5);
	//
	console.log(`Un utilisateur '${someUserid}' s'est connecté à: ${time}`);
	//
	socket.on('disconnect', () => {
		console.log(` ${someUserid} s'est deconnecté`);
	});

	socket.on('message', (msg) => {
		console.log('message: ' + msg);
		io.emit('message', `${someUserid} : ${msg}`);
	});
});
const PORT = process.env.PORT_SOCKET;
server.listen(PORT, () => {
	console.log('listening on : ', PORT_SOCKET);
});
